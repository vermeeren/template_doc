Index
=====

A few things to know when in a hurry and you already know the tooling:

* ``git submodule update --init``
* Use ``init.py`` to setup the project name, registry address, etc.
* Note the version of the template so updates with a diff are possible later.

.. toctree::
	:caption: Table of Contents
	:glob:

	*/index
