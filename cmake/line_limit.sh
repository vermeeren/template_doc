#!/bin/bash

# intermediate script to support extra options for line_limit.sh

# current script dir
SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"

# support WERROR option
[[ $WERROR == "ON" ]] && set -e

# call line_limit.sh to check repo
"$SCRIPT_DIR/../cicd/style/line_limit.sh" \
	-e '^build[^/]*/' \
	-e '^.*\.svg$' \
	-i "$SCRIPT_DIR/.."

exit 0
